import { MatDialogConfig } from '@angular/material/dialog';

export function defaultDialogConfig() {
  const dialogConfig = new MatDialogConfig();

  dialogConfig.disableClose = true;
  dialogConfig.autoFocus = true;
  dialogConfig.minWidth = '96%';
  dialogConfig.height = '85%';
  dialogConfig.position = {
    left: '50px',
  };

  return dialogConfig;
}
