declare const typeID: unique symbol;

export type Opaque<Identifier extends string, T> = T & {
  [typeID]: Identifier;
};
