import { Component, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import {
  NavigationCancel,
  NavigationEnd,
  NavigationError,
  NavigationStart,
  Router,
} from '@angular/router';
import { currentAccount } from './auth/store/auth.selectors';
import { logoutAction, refreshSessionAction } from './auth/store/auth.actions';
import { Account } from '#src/app/auth/models/account';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  loading = true;
  currentAccount$: Observable<Account | null>;

  constructor(private router: Router, private store: Store) {}

  ngOnInit() {
    this.store.dispatch(refreshSessionAction());

    this.router.events.subscribe(event => {
      switch (true) {
        case event instanceof NavigationStart: {
          this.loading = true;
          break;
        }

        case event instanceof NavigationEnd:
        case event instanceof NavigationCancel:
        case event instanceof NavigationError: {
          this.loading = false;
          break;
        }
        default: {
          break;
        }
      }
    });

    this.currentAccount$ = this.store.pipe(select(currentAccount));
  }

  logout() {
    this.store.dispatch(logoutAction());
  }
}
