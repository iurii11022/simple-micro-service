import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CookieService } from 'ngx-cookie-service';
import { environment } from '#src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class CookieStoreService {
  constructor(private http: HttpClient, private cookieService: CookieService) {}

  public getCookie(url: string): Observable<HttpResponse<any>> {
    return this.http.get<any>(url, { observe: 'response' });
  }

  public setCookie(name: string, value: string): void {
    this.cookieService.set(name, value);
  }

  public deleteSessionCookie(): void {
    this.cookieService.delete(environment.sessionCookie);
  }
}
