import { AccountId } from '#src/app/models/account-id';
import { Email } from '#src/app/models/email';

export type LogInnedAccountId = {
  account_id: AccountId;
  account_email: Email;
};
