import { Email } from '#src/app/models/email';
import { Password } from '#src/app/models/password';

export type AccountDataToLogIn = {
  email: Email;
  password: Password;
};
