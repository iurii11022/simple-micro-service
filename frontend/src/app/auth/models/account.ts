import { Email } from '#src/app/models/email';
import { AccountId } from '#src/app/models/account-id';
import { Nickname } from '#src/app/models/nickname';

export type Account = {
  account_id: AccountId;
  account_email: Email;
  account_nickname: Nickname;
};
