import { Email } from '#src/app/models/email';
import { Password } from '#src/app/models/password';
import { Nickname } from '#src/app/models/nickname';

export type AccountDataToRegister = {
  email: Email;
  password: Password;
  nickname: Nickname;
};
