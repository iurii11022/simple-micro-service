import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '#src/environments/environment';
import { AccountDataToRegister } from '#src/app/auth/models/account-data-to-register';
import { AccountDataToLogIn } from '#src/app/auth/models/account-data-to-login';
import { Account } from '#src/app/auth/models/account';

const apiUrls = {
  REGISTER: `${environment.backendApiUrl}/auth/sign-up`,
  LOGIN: `${environment.backendApiUrl}/auth/log-in`,
  REFRESH_SESSION: `${environment.backendApiUrl}/auth/refresh-session`,
  LOGOUT: `${environment.backendApiUrl}/auth/log-out`,
} as const;

@Injectable()
export class AuthHttpClient {
  constructor(private http: HttpClient) {}

  register(
    accountDataToRegister: AccountDataToRegister
  ): Observable<HttpResponse<Account>> {
    return this.http.post<HttpResponse<Account>>(apiUrls.REGISTER, {
      email: accountDataToRegister.email,
      password: accountDataToRegister.password,
      nickname: accountDataToRegister.nickname,
    });
  }

  logIn(
    accountDataToLogIn: AccountDataToLogIn
  ): Observable<HttpResponse<Account>> {
    return this.http.post<Account>(
      apiUrls.LOGIN,
      {
        email: accountDataToLogIn.email,
        password: accountDataToLogIn.password,
      },
      { observe: 'response' }
    );
  }

  refreshSession(): Observable<HttpResponse<Account>> {
    console.log('fddfsdfdsfdsfsdfsdffd');
    return this.http.post<Account>(apiUrls.REFRESH_SESSION, null, {
      observe: 'response',
    });
  }

  logOut(): Observable<HttpResponse<string>> {
    return this.http.post<string>(apiUrls.LOGOUT, null, {
      observe: 'response',
    });
  }
}
