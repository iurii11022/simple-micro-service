import { Component, OnInit } from '@angular/core';
import {
  UntypedFormBuilder,
  UntypedFormGroup,
  Validators,
} from '@angular/forms';

import { Store } from '@ngrx/store';

import { AuthHttpClient } from '../../clients/auth.http.client';
import { Router } from '@angular/router';
import { registerAction } from '../../store/auth.actions';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['../login-register.component.scss'],
})
export class RegisterComponent implements OnInit {
  form!: UntypedFormGroup;

  constructor(
    private fb: UntypedFormBuilder,
    private auth: AuthHttpClient,
    private router: Router,
    private store: Store
  ) {}

  ngOnInit() {
    this.initializeForm();
  }

  private initializeForm(): void {
    this.form = this.fb.group({
      email: [
        'test@angular.com',
        [Validators.required, Validators.email],
      ],
      password: ['test', [Validators.required]],
      nickname: ['nick', [Validators.required]],
    });
  }

  onSubmitRegisterForm(): void {
    const accountDataToRegisterProps = {
      accountDataToRegister: this.form.value,
    };

    this.store.dispatch(registerAction(accountDataToRegisterProps));
  }
}
