import { Component, OnInit } from '@angular/core';
import {
  UntypedFormBuilder,
  UntypedFormGroup,
  Validators,
} from '@angular/forms';

import { Store } from '@ngrx/store';

import { Router } from '@angular/router';
import { loginAction, registerAction } from '../../store/auth.actions';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['../login-register.component.scss'],
})
export class LoginComponent implements OnInit {
  form: UntypedFormGroup;

  constructor(
    private fb: UntypedFormBuilder,
    private router: Router,
    private store: Store
  ) {
    this.form = fb.group({
      email: ['test@angular.com', [Validators.required]],
      password: ['test', [Validators.required]],
    });
  }

  ngOnInit() {
    this.initializeForm();
  }

  private initializeForm(): void {
    this.form = this.fb.group({
      email: [
        'test@angular.com',
        [Validators.required, Validators.email],
      ],
      password: ['test', [Validators.required]],
    });
  }

  onSubmitLoginForm(): void {
    const accountDataToLogInProps = {
      accountDataToLogIn: this.form.value,
    };

    this.store.dispatch(loginAction(accountDataToLogInProps));
  }
}
