import { createAction, props } from '@ngrx/store';
import { Account } from '../models/account';
import {
  LogInActionType,
  LogOutActionType,
  refreshSessionActionType,
  RegisterActionTypes,
} from '#src/app/auth/store/models/auth.action-types';
import { AccountDataToRegister } from '#src/app/auth/models/account-data-to-register';
import { BackendError } from '#src/app/models/backend-errors';
import { AccountDataToLogIn } from '#src/app/auth/models/account-data-to-login';

export const loginAction = createAction(
  LogInActionType.LOGIN,
  props<{ accountDataToLogIn: AccountDataToLogIn }>()
);

export const loginSuccessAction = createAction(
  LogInActionType.LOGIN_SUCCESS,
  props<{ account: Account }>()
);

export const loginFailureAction = createAction(
  LogInActionType.LOGIN_FAILURE,
  props<{ errors: BackendError }>()
);

export const logoutAction = createAction(LogOutActionType.LOGOUT);
export const logoutSuccessAction = createAction(
  LogOutActionType.LOGOUT_SUCCESS
);

export const logoutFailureAction = createAction(
  LogOutActionType.LOGOUT_FAILURE,
  props<{ errors: BackendError }>()
);

export const registerAction = createAction(
  RegisterActionTypes.REGISTER,
  props<{ accountDataToRegister: AccountDataToRegister }>()
);

export const registerSuccessAction = createAction(
  RegisterActionTypes.REGISTER_SUCCESS,
  props<{ account: Account }>()
);

export const registerFailureAction = createAction(
  RegisterActionTypes.REGISTER_FAILURE,
  props<{ errors: BackendError }>()
);

export const refreshSessionAction = createAction(
  refreshSessionActionType.REFRESH_SESSION
);

export const refreshSessionSuccessAction = createAction(
  refreshSessionActionType.REFRESH_SESSION_SUCCESS,
  props<{ account: Account }>()
);

export const refreshSessionFailureAction = createAction(
  refreshSessionActionType.REFRESH_SESSION_FAILURE,
  props<{ errors: BackendError }>()
);
