import { createReducer, on } from '@ngrx/store';
import { AuthState } from '#src/app/auth/store/models/auth.state';
import {
  loginSuccessAction,
  logoutAction,
  refreshSessionSuccessAction,
} from '#src/app/auth/store/auth.actions';

export const initialAuthState: AuthState = {
  account: null,
};

export const authReducer = createReducer(
  initialAuthState,

  on(
    loginSuccessAction,
    (state, action): AuthState => ({
      ...state,
      account: action.account,
    })
  ),

  on(logoutAction, state => ({
    ...initialAuthState,
  })),

  on(
    refreshSessionSuccessAction,
    (state, action): AuthState => ({
      ...state,
      account: action.account,
    })
  )
);
