import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Router } from '@angular/router';
import { catchError, map, switchMap, tap } from 'rxjs/operators';

import {
  loginAction,
  loginFailureAction,
  loginSuccessAction,
  logoutAction,
  logoutFailureAction,
  logoutSuccessAction,
  refreshSessionAction,
  refreshSessionFailureAction,
  refreshSessionSuccessAction,
  registerAction,
  registerFailureAction,
  registerSuccessAction,
} from '#src/app/auth/store/auth.actions';
import { AuthHttpClient } from '#src/app/auth/clients/auth.http.client';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Account } from '#src/app/auth/models/account';
import { CookieStoreService } from '#src/app/global/services/cookie-store.service';
import { of } from 'rxjs';

@Injectable()
export class AuthEffects {
  constructor(
    private actions$: Actions,
    private router: Router,
    private authHttpClient: AuthHttpClient,
    private cookieService: CookieStoreService
  ) {}

  register = createEffect(() =>
    this.actions$.pipe(
      ofType(registerAction),
      switchMap(({ accountDataToRegister }) => {
        return this.authHttpClient.register(accountDataToRegister).pipe(
          map((response: HttpResponse<Account>) => {
            return registerSuccessAction({ account: response.body });
          }),
          catchError((errorResponse: HttpErrorResponse) => {
            return of(
              registerFailureAction({ errors: errorResponse.error.errors })
            );
          })
        );
      })
    )
  );

  login$ = createEffect(() =>
    this.actions$.pipe(
      ofType(loginAction),
      switchMap(({ accountDataToLogIn }) => {
        return this.authHttpClient.logIn(accountDataToLogIn).pipe(
          map((response: HttpResponse<Account>) => {
            return loginSuccessAction({ account: response.body });
          }),
          catchError((errorResponse: HttpErrorResponse) => {
            return of(
              loginFailureAction({ errors: errorResponse.error.errors })
            );
          })
        );
      })
    )
  );

  redirectAfterSubmit$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(loginSuccessAction, registerAction),
        tap(() => {
          this.router.navigateByUrl('/');
        })
      ),
    { dispatch: false }
  );

  refreshSession$ = createEffect(() =>
    this.actions$.pipe(
      ofType(refreshSessionAction),
      switchMap(() => {
        return this.authHttpClient.refreshSession().pipe(
          map((response: HttpResponse<Account>) => {
            console.log(response, 'refreshSession$----------------');
            return refreshSessionSuccessAction({ account: response.body });
          }),
          catchError((errorResponse: HttpErrorResponse) => {
            return of(
              refreshSessionFailureAction({
                errors: errorResponse.error.errors,
              })
            );
          })
        );
      })
    )
  );

  logout$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(logoutAction),
        switchMap(() => {
          return this.authHttpClient.logOut().pipe(
            map(() => {
              return logoutSuccessAction();
            }),
            catchError((errorResponse: HttpErrorResponse) => {
              return of(
                logoutFailureAction({ errors: errorResponse.error.errors })
              );
            })
          );
        })
      ),
    { dispatch: false }
  );
}
