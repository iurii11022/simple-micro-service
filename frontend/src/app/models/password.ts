import { Opaque } from '#src/utils/types/opaque';

export type Password = Opaque<string, 'Password'>;
