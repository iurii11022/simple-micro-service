import { Opaque } from '#src/utils/types/opaque';

export type Email = Opaque<string, 'Email'>;
