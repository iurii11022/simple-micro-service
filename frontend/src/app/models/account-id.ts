import { Opaque } from '#src/utils/types/opaque';

export type AccountId = Opaque<string, 'AccountId'>;
